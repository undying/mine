#! /bin/bash

pstree \
  -p $(ps axuwf|grep -v grep|grep cpuburn|awk '{print $2}') \
  |grep -o '[0-9]\+' \
  |xargs -L1 renice -n +19 -p

